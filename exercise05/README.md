# Exercise 05 - Add content using form fragments

## Objective
In this section you will learn how to add content to your form by using so-called form fragments. Also you will learn how to preview your form.

## Add content fragment
We will use a predefined adaptive form fragment to define the Addresses section. Adaptive form fragments are reusable form components that you can create once and then use in multiple forms. This allows you to quickly and easily insert common form elements in a form. Think for example an address block, consisting of street, postal code and city. You can define them in an address form fragment that you can then use across many forms.
For our form, we have prepared an address fragment and saved that in a form fragment which we will use in our Addresses section.
1. Select the **Addresses** component in *Content* pane.
2. Select **Assets** to open the *Assets* pane.
3. Select **Adaptive Form Fragments** from the *Images* dropdown listbox.
4. Drag and drop the **Address fragment** adaptive form framgment from the *Assets* pane and drop it onto the *Addresses* component in the form, as in the screenshot below.\
![Drag fragment on Start component](../images/start.png)

See this Youtube [video](https://youtu.be/UfkWNV76gTk) for a replay of above exercise.

## Allow for multiple persons
We would like to extend this form so multiple Addresses can be added for one identity. Rather than copying the steps above, we can set up the form to allow for repeating entries.

1. Go back to the *Content* pane by clicking on **Content**
2. Select the **Section** component. Click on the **Configure** button in the *Section* component's toolbar.
3. Rename panel *Name* to **pnlAddress**.
4. Set *Title* to **Address**.
5. Set *Repeat Settings - Minimum* to **1** and *Repeat Settings - Maximum* to **2**.
6. Click **√** to confirm.
7. Select the **Addresses** component. Click on the **Configure** button in the *Addresses* component's toolbar.
8. Select **Accordion** from the *Layout* dropdown list.
9. Click **√** to confirm.

Your form should now look like the screenshot below:\
![Repeat Person](../images/peoplerepeat.png)


## Preview the form
At any time you can preview a form to experience how it looks, feels and behaves for the end-user. To do so:

1. Click on **Preview** in the Page toolbar.
2. Click on the **Addresses** tab on the left.
3. Click on the **+** icon in the **Address** panel to add another *Address* panel

As you can try out, you can delete the additional or initial *Address* panel as well.

## Next
* Continue to [Exercise 06](../exercise06/)
